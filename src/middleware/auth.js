const jwt = require('jsonwebtoken')
const {
    getData
} = require('../../dataProvider.js')

module.exports = async function (req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send({
            error: 'Access denied. No token provided.'
        })
    }

    const token = req.headers.authorization.split(' ')[1]
    const tokenPayload = jwt.decode(token)
    let userSecretKey

    try {
        const { users } = await getData()

        for (let user of users) {
            if (user.login === tokenPayload.login) {
                userSecretKey = user.secretKey;
            }
        }

    } catch (e) {
        console.log(e)
        return res.status(500).send({
            error: "Server error."
        })
    }

    jwt.verify(token, userSecretKey, async (error, payload) => {
        if (error) {
            return res.status(401).send({
                error: 'Access denied. Invalid token.'
            })
        }

        req.user = payload

        next()
    })
}


