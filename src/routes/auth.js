const express = require('express')
const jwt = require('jsonwebtoken')

const {
    getData
} = require('../../dataProvider.js')

const router = express.Router()

router.post('/', async (req, res) => {
    const {
        login,
        password
    } = req.body
    try {
        const { users } = await getData()

        let user = users.find(user => login === user.login && password === user.password)

        if (user) {
            const { secretKey } = user
            delete user.secretKey
            delete user.password
            return res.json({
                success: true,
                message: {
                    token: jwt.sign(user, secretKey),
                    user
                }
            })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({
            success: false,
            message: error
        })
        return
    }

    res.status(401).json({
        success: false,
        message: 'Data is incorrect'
    })
})

module.exports = router
