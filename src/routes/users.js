const express = require('express')
const jwt = require('jsonwebtoken')
const isAdmin = require('../middleware/admin')
const isAuthed = require('../middleware/auth')


const {
    getData,
    changeData
} = require('../../dataProvider.js')

const router = express.Router()


router.get('/', [isAuthed, isAdmin], async (req, res, next) => {

    try {
        let {users} = await getData();

        users.forEach(user => {
            delete user.secretKey
            delete user.password

        })

        return res.json ( {
            success: true,
            users
        })
    } catch (error) {
        res.json({
            success: false,
            message: error
        })

    }
})


router.get('/profile', isAuthed, async (req, res, next) => {
   const {
        login
    } = req.user;


    try {
        let {users} = await getData();

        const user = users.find(user => user.login === login && user.password === user.password);

        return res.json ( {
            success: true,
            user
        })
    } catch (error) {
        res.json({
            success: false,
            message: error
        })

    }
})



router.post('/', async (req, res, next) => {
    const {
        login,
        password,
        name,
        surname,
        isAdmin = false,
        email
    } = req.body

    let data
    try {
        data = await getData()
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
        return
    }

    for (const key in data.users) {
        if (data.users[key].login === login) {
            return res.status(400).json({
                success: false,
                message: 'Username with that login already exists'
            })
        }
    }
    const secretKey = makeid(10)
    const user = {
        login,
        password,
        name,
        surname,
        secretKey,
        isAdmin,
        email
    }

    data.users.push(user)

    try {
        await changeData(data)
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
        return
    }

    delete user.secretKey
    delete user.password
    res.json({
        success: true,
        message: {
            token: jwt.sign(user, secretKey),
            user
        }
    })
})

const makeid = length => {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const charactersLength = characters.length
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
}

module.exports = router
